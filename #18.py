class Car:
    def __init__(self, model, year):
        self.model = model
        self.year = year
    
    def __str__(self):
        return f"{self.model} is {self.year} year of production"
        
    def Power(self, Pow):
        return f"{self.model} has {Pow} horsepower"
        
class MinPower(Car):
    def Power(self, Pow = "< 100"):
        return super().Power(Pow)

class AvgPower(Car):
    def Power(self, Pow = "100 > and < 300"):
        return super().Power(Pow)

class MaxPower(Car):
    def Power(self, Pow = "> 300"):
        return super().Power(Pow)
  
Lada = MinPower("Lada Vesta", 2011)      
Kia = AvgPower("Kia Spectra", 2007)
Bmw = MaxPower("BMW M7", 2021)

print(Lada.Power())
print(Kia.Power())
print(Bmw.Power())



