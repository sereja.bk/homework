count = 0

def my_dec(func):
    def wrapper():
        func()
    return wrapper

@my_dec 
def func_count():
    global count
    count += 1

func_count()
func_count()
func_count()

print(count)