class Car:
    def __init__(self, brand, model, year):
        self.brand = brand
        self.model = model
        self.year = year
        
car = Car("Kia", "Spectra", 2007)
print(car.brand, car.model, car.year)